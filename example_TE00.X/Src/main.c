/* 
 * File:   main.c
 * Author: Juan
 *
 * Created on 6 de julio de 2019, 03:13 PM
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "pic32_uart.h"

#define PIC32_BAUD_RATE 115200

uint8_t u8_first_var;
/*
 * 
 */
void main(void){
    UART1_init(PIC32_BAUD_RATE);
    u8_first_var = 0b00000101; //0x05
    PRINT_MESSAGE("The original number is %d \n", u8_first_var); //(00000101 = 5)
    u8_first_var ^= 0b00000001;
    PRINT_MESSAGE("Toggling the number with ^ ... actual number is %d \n", u8_first_var); //(00000100= 4);
    
    while(1){}
    
}


